#ifndef UI_H_
#define UI_H_
#include"Header.h"
#include <math.h>

void start(AVLtree &tree, AVLtree &stopword);
void end(AVLtree &tree, AVLtree &stopword);
void search(AVLtree &tree, AVLtree &stopword);
void history(AVLtree &tree, AVLtree &stopword);
void operate(AVLtree &tree, AVLtree &stopword);

#endif
