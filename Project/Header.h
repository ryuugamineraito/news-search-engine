#ifndef FUNCTION_H_
#define FUNCTION_H_
#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <cstring>
#include <time.h>

using namespace std;
//I've deleted paragraph structure and replace with int the index is the position and the value is the frequency
struct keyblock {
	string title;
	string wildcard;//just use when i use wildcard
	int frequency=1;
	int para[100];//use arr because I'm afraid of using pointer :)) 
	keyblock() { for (int i = 0; i < 100; ++i) para[i] = 0; };
};
struct node {
	string key;
	vector<keyblock> data;
	node* left;
	node* right;
	int height;
};
class AVLtree {
private:
	node * root;
	void traversePreorder(node* &N);
	void printPreorder(node* N);
	void printPostorder(node* N);
	void printInorder(node* N);
	node* insert(node* N, string keyword,keyblock data,int position);
	node *search(node *cur, string x);
	void removeAllInternall(node *&N);
public:
	AVLtree();
	void Sort();
	void removeAll();
	node* rotateLeft(node* x);
	node* rotateRight(node* y);
	void insert(string keyword,keyblock data,int position);
	int getHeight(node*N);
	int checkBalance(node* N);
	void printPreorder();
	void printPostorder();
	void printInorder();
	bool search(string x,node *&tmp);
	void LoadStopWord();
	void searchPriceAndHash(string key);
	void search_OR(string&text);
	void search_AND(string &text);
};
//some plugin function =))
int biggest(int a, int b);
void LoadFile(string path, AVLtree &a);
string itos(int n);
void fileprocess(ifstream &fin, AVLtree &a, string filename);
void lowercase(string &classname);
bool strcmpi(string source, string des);
void keyblockSort(vector<keyblock> &a);
vector<string> Filter(string in);
void CreateFile_Summary(string path, string a);
void ReLoadFile(string path, AVLtree&a);
void CreateFileHistory( string query);
void Check_Operator(string in, AVLtree&tree, AVLtree &stopword);
//input processing
vector<string> splitkeywords(string query, AVLtree &stopwords);
void normal_output(vector<string> input, AVLtree &a);
void merge(vector<keyblock>&result, vector<keyblock>query);
void print_vector(vector <keyblock> result, vector<string> query);
int max_index(int a[100]);
void print_vector2(vector <keyblock> result);
void start(AVLtree &tree, AVLtree &stopword);
void end(AVLtree &tree, AVLtree &stopword);
void search(string &query,AVLtree &tree, AVLtree &stopword);
void history(string &query,AVLtree &tree, AVLtree &stopword);
void operate(AVLtree &tree, AVLtree &stopword);
#endif