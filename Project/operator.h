#ifndef FUNC_H_
#define FUNC_H_
#include"Header.h"
bool isWildCard(string query);
bool isExact(string query);
void exactOps(string query, AVLtree &stopword, AVLtree &tree);
bool isExist(string keyword, keyblock &tmp);
void wildcardOps(string query, AVLtree &stopword, AVLtree &tree);
bool isMatch(string before, string after, keyblock &tmp);
bool is_txt(string text);
bool is_currency(string text);
bool is_hashtag(string text);
bool is_title(string text);
bool is_exclude(string text);
bool is_OR(string text);
bool is_included(string text);
bool is_title(string text);
bool isWildCard(string query);
bool is_included(string text);
bool is_AND(string text);
#endif