#include"operator.h"
#include"Header.h"
#include"Highlight.h"
bool isWildCard(string query)
{
	if (isExact(query))
	{
		if (query.find('*') != -1) return true;
		else return false;
	}
	return false;
}
//okay let write wild card function
void wildcardOps(string query, AVLtree &stopword, AVLtree &tree)
{
	vector<string>queries;
	string beforewild = query.substr(query.find('"') + 1, query.find('*') - 2);
	if (beforewild[0] == '*'||beforewild.empty())
		beforewild.clear();
	string afterwild = query.substr(query.find('*') + 2, query.find('"', 1) - query.find('*') - 2);
	if (afterwild[0] == '*'||afterwild.empty())
		afterwild.clear();
	if (beforewild.empty())
		queries = splitkeywords(afterwild, stopword);
	else
		queries = splitkeywords(beforewild, stopword);
	node*get = NULL;
	tree.search(queries[0], get);
	vector<keyblock> result;
	if (get != NULL)
	{
		for (int i = 0; i < get->data.size(); ++i)
		{
			keyblock tmp;
			tmp.title = get->data[i].title;
			if (isMatch(beforewild, afterwild, tmp))
				result.push_back(tmp);
			if (result.size() == 5)
				break;
		}
		if (!result.empty())
		{
			keyblockSort(result);
			print_vector2(result);
		}
	}
	else
		cout << "Results found:" << endl;
}
bool isMatch(string before,string after, keyblock &tmp)
{
	string path = "C:\\Users\\Light\\Downloads\\CS163-Project-Data\\CS163-Project-Data\\";
	ifstream fin; string fullpath = path + tmp.title + ".txt";
	fin.open(fullpath);
	string sample;
	int pos = 0;
	bool check = false;
	while (!fin.eof())
	{
		getline(fin, sample);
		lowercase(sample);
		if (!before.empty() && !after.empty())
			//normal case and case dont have after;
		{
			int posbeforewild = sample.find(before), posafterwild = sample.find(after);
			if (posbeforewild != -1 && posafterwild != -1)
			{
				int start = posbeforewild + before.length() + 1;
				string wildcard = sample.substr(start, sample.find(' ', start) - start);//read the next word after key before wild
				int sample = wildcard.length();
				int compare = posafterwild - 1 - start;
				if (sample == compare)//compare if the length of the word we've just read match with the gap between before wildcard and after wild card
				{
					tmp.wildcard = before + ' ' + wildcard + ' ' + after;
					tmp.frequency++;
					tmp.para[pos]++;
					check = true;
				}
			}//dont have after case
		}
		else if (after.empty())
		{
				int posbeforewild = sample.find(before);
				if (posbeforewild != -1)
				{
					int start = posbeforewild + before.length() + 1;
					string wildcard = sample.substr(start, sample.find(' ', start) - start);
					tmp.wildcard = before + ' ' + wildcard;
					tmp.frequency++;
					tmp.para[pos]++;
					check = true;
				}
		}
		else if (before.empty())
		{
			int posafterwild = sample.find(after);
			if (posafterwild != -1)
			{
				string keyword = sample.substr(0, posafterwild - 1);
				AVLtree dummy;
				vector<string>dumb = splitkeywords(keyword, dummy);
				string wildcard = dumb[dumb.size() - 1] + ' ' + after;
				dummy.removeAll();
				tmp.frequency++;
				tmp.para[pos]++;
				check = true;
			}
		}
		if (sample.empty())
			++pos;
	}
	fin.close();
	return check;
}
bool isExact(string query)
{
	int start = query.find('"');
	int end = query.find('"', start + 1);
	if (start != -1 && end != -1)
		return true;
	else return false;
}
//write some function
bool isExist(string keyword,keyblock &tmp)
{
	string path = "C:\\Users\\Light\\Downloads\\CS163-Project-Data\\CS163-Project-Data\\";
	ifstream fin; string fullpath = path + tmp.title + ".txt";
	fin.open(fullpath);
	string sample;
	int pos = 0;
	bool check = false;
	while (!fin.eof())
	{
		getline(fin, sample);
		lowercase(sample);
		if (sample.empty())
			++pos;
		if (sample.find(keyword) != -1)
		{
			tmp.frequency++;
			tmp.para[pos]++;
			check = true;
		}
	}
	fin.close();
	return check;
}
void exactOps(string query,AVLtree &stopword, AVLtree &tree)
{
	//filter the word after the first "
	string keyword = query.substr(query.find('"') + 1, query.find('"', query.find('"') + 1) - query.find('"')- 1);
	vector<string>queries = splitkeywords(keyword, stopword);
	node*get = NULL;
	tree.search(queries[0], get);
	vector<keyblock> result;
	for (int i = 0; i < get->data.size(); ++i)
	{
		keyblock tmp;
		tmp.title = get->data[i].title;
		if (isExist(keyword, tmp))
			result.push_back(tmp);
		if (result.size() == 5)
			break;
	}
	keyblockSort(result);
	print_vector(result, queries);
	get = NULL; delete get;
}
//
bool is_txt(string text) {
	int txt = text.find(":txt");
	if (txt <= 0 || txt>text.size())return false;
	if (text[txt - 1] == ' ')return false;
	else return true;
}
bool is_currency(string text) {
	int money = text.find("$");
	if (money < 0 || money>text.size())return false;
	if (text.size() == money + 1)return false;
	if (isdigit(text[money + 1]))return true;
	else return false;
}
bool is_hashtag(string text) {
	int tag = text.find("#");
	if (tag < 0 || tag > text.size()) return false;
	if (text.size() == tag + 1)return false;
	if (text[tag + 1] != ' ')return true;
	else return false;
}
bool is_title(string text) {
	int tit = text.find("intitle:");
	if (tit != 0)return false;
	for (int i = tit + 8; i < text.size(); i++)
		if (text[i] != ' ')return true;
	return false;
}

bool is_exclude(string text) {
	bool fw = false;
	int excl = text.find("-");
	if (excl <= 0 || excl > text.size())return false;
	if (text[excl - 1] != ' ')return false;
	for (int i = 0; i < excl - 1; i++)
		if (text[i] != ' ') fw = true;
	for (int i = excl + 1; i < text.size() && fw == true; i++)
		if (text[i] != ' ') return true;
	return false;
}

bool is_OR(string text) {
	bool fw = false;//first word
	int And = -1;//or operator position
	for (int i = 0; i < text.size(); i++) {
		if (text[i] != ' ' && fw == false) {
			fw = true;
			And = text.find("OR", i + 1);
			if (And < 0 || And > text.size())return false;
			else {
				i += And + 2;
				if (i > text.size())return false;
			}
		}
		if (And != -1 && text[i] != ' ')return true;
	}
	return false;
}

bool is_included(string text) {
	bool fw = false;
	int incl = text.find("+");
	if (incl <= 0 || incl > text.size() || text[incl - 1] != ' ') return false;
	for (int i = 0; i < incl - 1; ++i)
		if (text[i] != ' ') fw = true;
	for (int i = incl + 1; i < text.size() && fw == true; i++)
		if (text[i] != ' ') return true;
	return false;
}
bool is_AND(string text) {
	bool fw = false;//first word
	int And = -1;//and operator position
	for (int i = 0; i < text.size(); i++) {
		if (text[i] != ' ' && fw == false) {
			fw = true;
			And = text.find("AND", i + 1);
			if (And < 0 || And > text.size())return false;
			else {
				i += And +3;
				if (i > text.size())return false;
			}
		}
		if (And != -1 && text[i] != ' ')return true;
	}
	return false;
}